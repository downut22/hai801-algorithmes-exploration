#include <cstdlib>
#include <iostream>
#include <ctime>
#include <vector>
#include <queue>
#include <fstream>
#include <string>

#include "../Library/tree.h"

using namespace std;

class Plateau
{
private:
	
	int* table;

public:
	bool finished;
	int turn;
	int size; int taille;

	int get(int i)
	{
		return table[i];
	}

	int get(int x, int y)
	{
		return get(y*taille+x);
	}

	bool set(int i, int val)
	{
		if(i < 0 || i >= size){return false;}
		if(table[i] != 0){return false;}
		table[i] = val;
		return true;
	}

	bool set(int x, int y, int val)
	{
		return set(y*taille+x,val);
	}

	int lineValue(int i)
	{
		int c = 0;
		for(int a = 0 ; a < taille; a++)
		{
			c+= get(a,i);
		}
		return c;//get(0,i) + get(1,i) + get(2,i);
	}
	int  colValue(int i)
	{
		int c = 0;
		for(int a = 0 ; a < taille; a++)
		{
			c+= get(i,a);
		}
		return c; //get(i,0) + get(i,1) + get(i,2);
	}
	int diagValue(int i)
	{
		if(i == 0)
		{
			int c = 0;
			for(int a = 0 ; a < taille; a++)
			{
				c+= get(a,a);
			}
			return c;//get(0,0) + get(1,1) + get(2,2);
		}
		else
		{
			int c = 0;
			for(int a = 0 ; a < taille; a++)
			{
				c+= get(taille-1-a,a);
			}
			return c;
			//return get(2,0) + get(1,1) + get(0,2);
		}
	}

	int heuristique()
	{
		int e = 0;

		for(int i = 0 ; i < taille; i++)
		{
			e += lineValue(i);
			e += colValue(i);
		}
	
		e += diagValue(0);
		e += diagValue(1);

		return e;
	}

	int win()
	{
		for(int i = 0 ; i < taille; i++)
		{
			if(abs(lineValue(i)) >= taille) return lineValue(i);
			if(abs(colValue(i)) >= taille) return colValue(i);
		}
	
		if(abs(diagValue(0)) >= taille) return diagValue(0);
		if(abs(diagValue(1)) >= taille) return diagValue(1);

		return 0;
	}

	const char* draw(int val)
	{
		return val == 0 ? " " : val > 0 ? "X" : "O";
	}

	void display()
	{
		cout << endl << "\t\t\tTurn : " << turn << "\t\theuristique : " << heuristique() << endl << endl;

		cout << "\t\t\t\t\t" << diagValue(0) << "\t";
		for(int i = 0; i < taille; i++)
		{
			cout << colValue(i) << "\t";
		}
		cout << diagValue(1) << endl;

		cout << "\t\t\t\t";
		for(int i = 0; i < taille; i++){cout << "-----------" ;} cout << endl;

		for(int y = 0; y < taille; y++)
		{
			cout << "\t\t\t\t" << lineValue(y) << "\t|\t"; 
			for(int x = 0; x < taille; x++)
			{
				cout << draw(get(x,y)) << "\t";
			}
			cout << endl;
			cout << "\t\t\t\t\t|" << endl;
		}

		cout << "\t\t\t\t";
		for(int i = 0; i < taille; i++){cout << "-----------" ;} cout << endl;

		/*cout << "\t" << diagValue(0) << "\t" << colValue(0) << "\t" << colValue(1) << "\t" << colValue(2) << "\t" << diagValue(1) << endl;
		cout << "------------------------------------" << endl;
		cout << lineValue(0) << "\t|\t" << draw(table[0]) << "\t" << draw(table[1]) << "\t" << draw(table[2]) << "\t" << endl;
		cout << "\t|" << endl;
		cout << lineValue(1) << "\t|\t" << draw(table[3]) << "\t" << draw(table[4]) << "\t" << draw(table[5]) << "\t" << endl;
		cout << "\t|" << endl;
		cout << lineValue(2) << "\t|\t" << draw(table[6]) << "\t" << draw(table[7]) << "\t" << draw(table[8]) << "\t" << endl;
		cout << "-----------------------------------" <<  endl;
		*/
		cout << endl;
	}

	Plateau(Plateau* from)
	{
		table = new int[from->size];
		for(int i = 0; i < from->size; i++){table[i] = from->get(i);}
		turn = from->turn;
		this->size = from->size;
		this->taille = from->taille;
	}

	Plateau(int taille)
	{
		this->size = taille*taille;
		table = new int[size];
		for(int i = 0; i < size; i++){table[i] = 0;}
		turn = 0;
		this->taille = taille;
	}
};

Plateau* plateau;


Node* buildTree(int deep, int lastMove, int turn, Plateau* plat, int maxDeep)
{
	if(maxDeep <= 0) {return new Node(plat->heuristique() , 0 , deep , lastMove );}

	if(plat->win() < 0) { return new Node(deep - 10,0,deep,lastMove);}  
	if(plat->win() > 0) { return new Node(10 - deep,0,deep,lastMove);} 
	if(deep >= plat->size) {	return new Node(0,0,deep,lastMove);}

	Node* node = new Node(0,9-deep,deep,lastMove);

	int move = 0;
	for(int i = 0; i < plat->size - deep; i++)
	{
		Plateau* nPlat = new Plateau(plat); nPlat->turn++;

		while(!nPlat->set(move,turn))
		{
			move++;
		}

		node->childs.push_back(buildTree(deep+1,move,-turn,nPlat,maxDeep-1));

		node->value += node->childs.at(node->childs.size()-1)->value;
		/*if(turn > 0)
		{
			node->value = max(node->value , node->childs.at(node->childs.size()-1)->value);
		}
		else
		{
			node->value = min(node->value , node->childs.at(node->childs.size()-1)->value);
		}*/

		move++;
	}

	return node;
}

int maxDeep = 100;

int AImove(int lastMove)
{
	Node* root = buildTree(plateau->turn,lastMove,1,plateau,maxDeep);

	cout << "\t\tPossible moves N° :\t" ;

	for(int i = 0; i < root->childs.size(); i++)
	{
		cout << root->childs.at(i)->id << "  |  ";
	}
	cout << endl;

	cout << "\t\tCorresponding scores : " ;

	int move = 0; int score = -100000000;
	for(int i = 0; i < root->childs.size(); i++)
	{
		cout << root->childs.at(i)->value << "  |  ";
		if(root->childs.at(i)->value > score)
		{
			score = root->childs.at(i)->value;
			move = root->childs.at(i)->id;
		}
	}

	cout << endl;

	return move;
}

int playerTurn()
{
 	int x; int y; int i;

 	do
 	{
		cout << endl << "\tPlayer : Enter your move (x y) : ";
		scanf("%i %i",&x,&y);
		i =  x + y*plateau->taille;

		cout <<  endl << "\t\t  Player plays at position n°" << i << " :  x:" << x << "  y:" << y << endl;
	}
	while(!plateau->set(x,y,-1));

	return i;
}

int AIturn(int lastMove)
{
	cout << endl << "\tAI is thinking ... " << endl;
	
	int move = AImove(lastMove);

	int x = move % plateau->taille; int y = move / plateau->taille;

	cout << endl << "\t\t  AI plays at position n°" << move << " : x:" << x << "  y:" << y << endl;

	plateau->set(x,y,1);

	return move;
}

int main(int argc, char **argv)
{
	std::srand(std::time(nullptr));

	int order;
	if(argc < 2){order = 0;}
	else{
		sscanf(argv[1],"%i",&order);
	}

	int size;
	if(argc < 3){ size = 3; }
	else{
		sscanf(argv[2],"%i",&size);
	}

	if(argc < 4){  }
	else{
		sscanf(argv[3],"%i",&maxDeep);
	}

	cout << "Generating plate " << size << "x" << size << endl;

	cout << (order == 0 ? "Player begins" : "AI begins") << endl;

	plateau = new Plateau(size);

	int lastMove;
	while(!plateau->finished)
	{
		plateau->display();

		if( (plateau->turn%2==0 && order == 0) || (plateau->turn%2!=0 && order != 0))
		{
			lastMove = playerTurn();
		}
		else
		{
			lastMove = AIturn(lastMove);
		}

		int w = plateau->win();
		if(w != 0)
		{
			plateau->display();
			cout << ((w > 0) ? "AI" : "Player") << " won" << endl;
			return 0;
		}

		plateau->turn++;

		if(plateau->turn > (size*size)-1)
		{
			plateau->display();
			cout << "DRAW" << endl;
			return 0;
		}
	}

	return 0;	
}