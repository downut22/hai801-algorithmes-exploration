
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <vector>
#include <queue>
#include <fstream>
#include <string>
#include <stack>
using namespace std;

struct Node
{
	int value;
	int deep;
	vector<Node*> childs; int childCount;
	int id;

	bool isLeaf() { return childs.size() <= 0; }

	Node(){}

	Node(int _value, int _childCount, int _deep, int _id)
	{
		value = _value;
		deep = _deep;
		id = _id;
		childs = vector<Node*>();

		if(_childCount > 0)
		{
			childs.resize(childCount);
		}
	}
};

Node* generateTree(int deepness, int width, int maxVal, int deep, int id)
{
	int value = (int)((std::rand()/(float)RAND_MAX) * maxVal);

	if(deepness <= 0)
	{
		return new Node(value,0,deep,id);	//Final leaf
	}
	else
	{
		int childCount = (int)((std::rand()/(float)RAND_MAX) * width) ;

		Node* node = new Node(value,0,deep,id);

		if(childCount > 0)
		{
			for(int i = 0; i < childCount; i++)
			{
				node->childs.push_back(generateTree(deepness-1,width,maxVal,deep+1,i));
			}
		}
		else
		{
			return new Node(value,0,deep,id);	//Leaf
		}

		return node;
	}
}

Node* readTree(vector<string> data)
{
	Node* root;
	stack<Node*> nodes;

	for(int i = 0; i < data.size()-1; i++)
	{
		int depth = 0; int valueCharSize = 1; char valueChar[100];
		for(int c = 0; c < data.at(i).size(); c++)
		{
			if(data.at(i)[c] == '-')
			{
				depth++;
			}
			else
			{
				valueChar[valueCharSize-1] = data.at(i)[c];
				valueCharSize++;
			}
		}
		int value; sscanf(valueChar, "%d", &value);

		if(depth == 0)
		{
			root = new Node(value,0,depth,0);
			nodes.push(root);
		}
		else
		{
			if(depth <= nodes.top()->deep)
			{
				for(int d = nodes.top()->deep; d >= depth; d--)
				{
					nodes.pop();
				}
			}
			Node* node = new Node(value,0,depth,nodes.top()->childs.size());
			nodes.top()->childs.push_back(node);
			nodes.push(node);
		}
	}

	return root;
}

void displayTree(Node* node)
{

	for(int i = 0; i < node->deep;i++)
	{
		std::cout << "	";
	}

	std::cout << node->value << std::endl;

	for(int i = 0; i < node->childs.size(); i++)
	{
		displayTree(node->childs[i]);
	}
}

bool findValueDFS(Node* node,int value, std::vector<int>& path)
{
	if(node->value == value)
	{
		return true;
	}
	else if(node->childs.size() > 0)
	{
		for(int i = 0; i < node->childs.size(); i++)
		{
			path.push_back(i); //std::cout << i << " ";
			if(findValueDFS(node->childs[i],value,path))
			{
				return true;
			}
			else
			{
				path.pop_back(); //std::cout << "pop ";
			}
		}
		return false;
	}
	else
	{
		return false;
	}
}

bool findValueBFS(Node* root, int value, std::vector<int>& path)
{
	std::queue<std::pair<Node*, std::vector<int>>> q;

    q.push(std::make_pair(root, std::vector<int>()));

    while (!q.empty())
    {
        Node* node = q.front().first;

        std::vector<int> cpath = q.front().second;

        q.pop();

        if (node->value == value)
        {
        	for(int i = 0; i < cpath.size(); i++){path.push_back(cpath.at(i));}
            return true;
        }

        for (int i = 0; i < node->childs.size(); i++)
        {
            std::vector<int> new_path(cpath);

            new_path.push_back(i);

            q.push(std::make_pair(node->childs[i], new_path));
        }
    }

    return false;
}

Node* tree;

int main(int argc, char **argv)
{
	std::srand(std::time(nullptr));

	int maxVal = 99;

	if(argc >= 2)
	{
		fstream treeFile;
   		treeFile.open(argv[1],ios::in);

   		if(treeFile.is_open())
   		{   		
   			vector<string> data;
   			string line;
   			while(treeFile.good())
   			{
   				treeFile >> line;
   				data.push_back(line);
   			}
   			tree = readTree(data);
   		}
   		else{return 0;}
  	}
  	else
  	{
		tree = generateTree(5,4,maxVal,0,0);
	}

	displayTree(tree);

	std::vector<int> pathDFS; 

	int value; std::cin >> value;//(int)((std::rand()/(float)RAND_MAX) * maxVal);

	std::cout << "DFS searching for " << value << std::endl;

	std::cout << (findValueDFS(tree,value,pathDFS) ? "Found" : "Not found") << std::endl;
	std::cout << "Chemin pour trouver la valeur : l'indice du noeud enfant où aller" << std::endl;
	//std::cout << "Exemple : 0 1 2 > on part de la racine, on entre dans son premier fils. Ensuite, on entre dans le second fils. Enfin, on entre dans le troisieme fils, et on y trouve la valeur cherchée" << std::endl;

	for(int i = 0; i < pathDFS.size(); i++)
	{
		std::cout << pathDFS.at(i) << " ";
	}
	std::cout << std::endl;

	std::vector<int> pathBFS;

	std::cout << std::endl << "BFS searching for " << value << std::endl;

	std::cout << (findValueBFS(tree,value,pathBFS) ? "Found" : "Not found") << std::endl;
	std::cout << "Chemin pour trouver la valeur : " << std::endl;

	for(int i = 0; i < pathBFS.size(); i++)
	{
		std::cout << pathBFS.at(i) << " ";
	}
	std::cout << std::endl;
}
